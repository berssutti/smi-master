import time

import requests
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers, status, viewsets
from rest_framework.exceptions import APIException, NotAcceptable
from rest_framework.response import Response

from campi.models import Campus
from slaves.models import Slave

from .api import check_connection, create_transductor, delete_transductor, update_transductor
from .models import EnergyTransductor


class EnergyTransductorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = EnergyTransductor
        fields = (
            "id",
            "serial_number",
            "ip_address",
            "port",
            "slave_server",
            "geolocation_latitude",
            "geolocation_longitude",
            "campus",
            "firmware_version",
            "name",
            "broken",
            "active",
            "model",
            "grouping",
            "firmware_version",
            "url",
            "history",
        )

        read_only_fields = ("active", "broken")

    def create(self, validated_data):
        existent_group_type = [group.type.name for group in validated_data.get("grouping")]
        valid_groups_type = set(existent_group_type)

        if len(existent_group_type) is not len(valid_groups_type):
            raise NotAcceptable(
                _("You could not link the same transductor for " "two or more groups of the same type.")
            )
        else:
            id_in_slave = 0
            if validated_data.get("slave_server") is not None:
                try:
                    slave_server = validated_data.get("slave_server")
                    response = create_transductor(validated_data, slave_server)
                    print(response.status_code)
                    if response.status_code != 201:
                        error_message = _("The collection server %s is unavailable" % slave_server.name)
                        exception = APIException(error_message)
                        exception.status_code = 400
                        raise exception
                    r = response.json()
                    print(r)
                    id_in_slave = r["id"]
                except Exception:
                    error_message = _("Could not connect with server %s. Try it again later" % slave_server.name)
                    exception = APIException(error_message)
                    exception.status_code = 400
                    raise exception

            else:
                id_in_slave = None
            transductor = EnergyTransductor.objects.create(
                serial_number=validated_data.get("serial_number"),
                ip_address=validated_data.get("ip_address"),
                firmware_version=validated_data.get("firmware_version"),
                campus=validated_data.get("campus"),
                name=validated_data.get("name"),
                model=validated_data.get("model"),
                geolocation_latitude=validated_data.get("geolocation_latitude"),
                geolocation_longitude=validated_data.get("geolocation_longitude"),
                slave_server=validated_data.get("slave_server"),
                id_in_slave=id_in_slave,
                history=validated_data.get("history"),
            )

            for group in validated_data.get("grouping"):
                transductor.grouping.add(group)

            return transductor

    def update(self, instance, validated_data):
        existent_group_type = [group.type.name for group in set(validated_data.get("grouping"))]
        valid_groups_type = set(existent_group_type)

        if len(existent_group_type) is not len(valid_groups_type):
            raise NotAcceptable(
                _("You could not link the same transductor for " "two or more groups of the same type.")
            )
        old_slave_server = instance.slave_server
        new_slave_server = validated_data.get("slave_server")

        errors = []
        if new_slave_server != old_slave_server:
            if not check_connection(old_slave_server):
                error_message = _("Could not disconnect from server %s. Try it again later" % old_slave_server.name)
                errors.append(error_message)

            if not check_connection(new_slave_server):
                error_message = _("Could not connect with server %s. Try it again later" % new_slave_server.name)

                errors.append(error_message)

            if errors.__len__() > 0:
                exception = APIException(errors)
                exception.status_code = 400
                raise exception

            else:
                try:
                    if old_slave_server is not None:
                        delete_transductor(instance.id_in_slave, instance, old_slave_server)

                except Exception:
                    error_message = _(
                        "Could not disconnect from server %s." " Try it again later" % old_slave_server.name
                    )

                    errors.append(error_message)
                try:
                    if new_slave_server is not None:
                        response = create_transductor(validated_data, new_slave_server)
                        r = response.json()
                        instance.id_in_slave = r["id"]

                except Exception:
                    error_message = _("Could not connect with server %s. Try it again later" % new_slave_server.name)

                    errors.append(error_message)

        else:
            try:
                if old_slave_server is not None:
                    update_transductor(instance.id_in_slave, validated_data, old_slave_server)

            except Exception:
                error_message = _("Could not connect with server %s. Try it again later" % new_slave_server.name)

                errors.append(error_message)

        if errors.__len__() > 0:
            exception = APIException(errors)
            exception.status_code = 400
            raise exception

        instance.serial_number = validated_data.get("serial_number")
        instance.ip_address = validated_data.get("ip_address")
        instance.port = validated_data.get("port")
        instance.history = validated_data.get("history")
        instance.firmware_version = validated_data.get("firmware_version")
        instance.campus = validated_data.get("campus")
        instance.name = validated_data.get("name")
        instance.model = validated_data.get("model")
        instance.geolocation_latitude = validated_data.get("geolocation_latitude")
        instance.geolocation_longitude = validated_data.get("geolocation_longitude")

        instance.grouping.set(validated_data.get("grouping"))

        instance.slave_server = new_slave_server

        instance.save()
        return instance


class AddToServerSerializer(serializers.Serializer):
    slave_id = serializers.IntegerField()


class EnergyTransductorListSerializer(serializers.HyperlinkedModelSerializer):
    current_precarious_events_count = serializers.IntegerField()
    current_critical_events_count = serializers.IntegerField()
    events_last72h = serializers.IntegerField()
    campus = serializers.CharField()
    grouping = serializers.ListField(child=serializers.CharField())

    class Meta:
        model = EnergyTransductor
        fields = (
            "id",
            "serial_number",
            "campus",
            "name",
            "active",
            "model",
            "grouping",
            "current_precarious_events_count",
            "current_critical_events_count",
            "events_last72h",
        )
